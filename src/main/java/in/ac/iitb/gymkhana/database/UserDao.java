package in.ac.iitb.gymkhana.database;

import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.mappers.UserMapper;
import in.ac.iitb.gymkhana.model.User;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

/**
 * Created by akash on 29/4/15 at 5:04 AM.
 */

@RegisterMapper(UserMapper.class)
public interface UserDao {

    @SqlUpdate("insert into users(user_name, user_nfc_uid,user_roll_no, " +
            "user_hostel, created_at) values(:user_name,:user_nfc_uid,:user_roll_no," +
            ":user_hostel,NOW()) ")
    void addUser(@Bind(Constants.COLUMN_USER_NAME) String userName,
                 @Bind(Constants.COLUMN_USER_NFC_UID) double userNfcUid,
                 @Bind(Constants.COLUMN_USER_ROLL_NO) String userRollNo,
                 @Bind(Constants.COLUMN_USER_HOSTEL) String userHostel);

    @SqlQuery("select * from users  where user_nfc_uid=:user_nfc_uid")
    User getUser(@Bind(Constants.COLUMN_USER_NFC_UID) double userNfcUid);



}
