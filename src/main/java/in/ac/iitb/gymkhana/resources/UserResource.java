package in.ac.iitb.gymkhana.resources;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.database.CycleDao;
import in.ac.iitb.gymkhana.database.UserDao;
import in.ac.iitb.gymkhana.exception.ResponseMessageObject;
import in.ac.iitb.gymkhana.model.Cycle;
import in.ac.iitb.gymkhana.model.User;
import io.dropwizard.hibernate.UnitOfWork;
import org.skife.jdbi.v2.DBI;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by akash on 29/4/15 at 3:11 PM.
 */

@Path("/user")
@Consumes("application/json")
@Produces("application/json")
public class UserResource {

    private final UserDao userDao;
    private final CycleDao cycleDao;

    public UserResource(DBI jdbi) {
        userDao = jdbi.onDemand(UserDao.class);
        cycleDao = jdbi.onDemand(CycleDao.class);
    }

    @GET
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/{user_nfc_uid}")
    public Response getUserDetails(@PathParam(Constants.COLUMN_USER_NFC_UID) double userNfcUid) {
        User user = userDao.getUser(userNfcUid);
        if (user!=null){
            return Response.ok(user).build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ResponseMessageObject(Constants.STATUS_ERROR,
                        "No user found with that uid")).build();
    }

    @GET
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/{user_nfc_uid}/cycle")
    public Response getIssuedCycle(@PathParam(Constants.COLUMN_USER_NFC_UID) double userNfcUid){
        User user = userDao.getUser(userNfcUid);
        if(user!=null){
            Cycle cycle = cycleDao.getIssuedCycle(user.getUserId());
            if (cycle!=null){
                return Response.ok(cycle).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND)
                        .entity(new ResponseMessageObject(Constants.STATUS_ERROR,
                                "No cycle issued for user")).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ResponseMessageObject(Constants.STATUS_ERROR,
                        "No user found with that uid")).build();
    }

    @POST
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/create")
    public Response addUser(User user){
        User existingUser = userDao.getUser(user.getUserNfcUid());
        if (existingUser==null){
            userDao.addUser(user.getUserName(),user.getUserNfcUid()
                    ,user.getUserRollNo(),user.getUserHostel());
            return Response.status(Response.Status.fromStatusCode(201))
                    .entity(new ResponseMessageObject(Constants.STATUS_SUCCESS,
                            "User added to database")).build();
        }
        return Response.status(Response.Status.CONFLICT)
                .entity(new ResponseMessageObject(Constants.STATUS_ERROR,
                        "User already exists")).build();
    }

}
