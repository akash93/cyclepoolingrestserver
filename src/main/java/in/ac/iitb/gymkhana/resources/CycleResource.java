package in.ac.iitb.gymkhana.resources;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.database.CycleDao;
import in.ac.iitb.gymkhana.database.UserDao;
import in.ac.iitb.gymkhana.exception.ResponseMessageObject;
import in.ac.iitb.gymkhana.model.Cycle;
import in.ac.iitb.gymkhana.model.User;
import io.dropwizard.hibernate.UnitOfWork;
import org.skife.jdbi.v2.DBI;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by akash on 28/4/15 at 4:58 AM.
 */
@Path("/cycle")
@Produces("application/json")
@Consumes("application/json")
public class CycleResource{

    //TODO insert into logs tables for all actions

    private final CycleDao cycleDao;
    public CycleResource(DBI jdbi) {
        cycleDao = jdbi.onDemand(CycleDao.class);
    }



    @GET
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/{cycle_nfc_uid}")
    public Response getCycleDetails(@PathParam(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid) {
        Cycle cycle = cycleDao.getCycle(cycleNfcUid);
        if(cycle==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response
                .ok(cycle)
                .build();
    }

    @POST
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/create")
    public Response addCycle(Cycle request){
        Cycle cycle = cycleDao.getCycle(request.getCycleNfcUid());
        if (cycle==null){
            cycleDao.addCycle(request.getCycleNfcUid(),
                    Constants.CYCLE_STATUS_AVAILABLE,request.getLocatedAt());
            return Response.status(Response.Status.fromStatusCode(201)).entity(
                    new ResponseMessageObject(Constants.STATUS_SUCCESS,
                            "Cycle added to database")
            ).build();
        }

        return Response.status(Response.Status.fromStatusCode(409)).entity(
                new ResponseMessageObject(Constants.STATUS_ERROR,
                        "Cycle already exists in database")
        ).build();
    }

    @PUT
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/{cycle_nfc_uid}/issue")
    public Response issueCycle(@PathParam(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid,Cycle request){
        Cycle cycle = cycleDao.getCycle(request.getCycleNfcUid());
        if (cycle!=null){
            cycleDao.issueCycle(request.getIssuedToId(), request.getExpectedBack(),
                    request.getIssueLocation(), request.getCycleNfcUid(),
                    Constants.CYCLE_STATUS_ISSUED);
            return Response.ok(new ResponseMessageObject(Constants.STATUS_SUCCESS, "Cycle issued to user")).build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ResponseMessageObject(Constants.STATUS_ERROR,
                        "No cycle found with that id")).build();

    }

    @PUT
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/{cycle_nfc_uid}/return")
    public Response returnCycle(@PathParam(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid,
                                Cycle request){
        Cycle cycle = cycleDao.getCycle(cycleNfcUid);
        if (cycle!=null){
            cycleDao.returnCycle(Constants.CYCLE_STATUS_AVAILABLE,request.getLocatedAt(),
                    cycleNfcUid);
            Timestamp expectedBack =  cycle.getExpectedBack();
            Timestamp currentTime = new Timestamp((new Date()).getTime());
            if (currentTime.after(expectedBack)){
                //TODO return expected time as well
                return Response.ok(new ResponseMessageObject(Constants.STATUS_ERROR
                        ,"Cycle overdue")).build();
            }else {
                return Response.ok(new ResponseMessageObject(Constants.STATUS_SUCCESS,
                        "Cycle successfully returned")).build();
            }

        }
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ResponseMessageObject(Constants.STATUS_ERROR,
                        "No cycle found with that id")).build();
    }


    @PUT
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/{cycle_nfc_uid}/report")
    public Response reportCycle(@PathParam(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid,
                                Cycle request) {
        Cycle cycle = cycleDao.getCycle(cycleNfcUid);
        if (cycle!=null) {
            cycleDao.reportCycle(Constants.CYCLE_STATUS_REPAIR,
                    request.getCycleProblem(),request.getLocatedAt(), cycleNfcUid);
            return Response.ok(new ResponseMessageObject(Constants.STATUS_SUCCESS,
                            "Cycle reported for repair")).build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ResponseMessageObject(Constants.STATUS_ERROR,
                        "No cycle found with that id")).build();

    }

    @PUT
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/{cycle_nfc_uid}/fix")
    public Response fixCycle(@PathParam(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid,
                           Cycle request) {
        Cycle cycle = cycleDao.getCycle(cycleNfcUid);
        if (cycle!=null) {
            cycleDao.fixCycle(Constants.CYCLE_STATUS_AVAILABLE,request.getLocatedAt(),
                    cycleNfcUid);
            return Response.ok(new ResponseMessageObject(Constants.STATUS_SUCCESS,
                    "Cycle marked as fixed")).build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ResponseMessageObject(Constants.STATUS_ERROR,
                        "No cycle found with that id")).build();
    }


}
