package in.ac.iitb.gymkhana.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.dropwizard.jackson.JsonSnakeCase;

/**
 * Created by akash on 29/4/15 at 4:59 AM.
 */

@JsonSnakeCase
@JsonIgnoreProperties
public class User {

    private int userId;
    private double userNfcUid;
    private String userName;
    private String userRollNo;
    private String userHostel;

    public User() {
    }

    public User(int userId, double userNfcUid, String userName,
                String userRollNo, String userHostel) {
        this.userId = userId;
        this.userNfcUid = userNfcUid;
        this.userName = userName;
        this.userRollNo = userRollNo;
        this.userHostel = userHostel;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getUserNfcUid() {
        return userNfcUid;
    }

    public void setUserNfcUid(double userNfcUid) {
        this.userNfcUid = userNfcUid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRollNo() {
        return userRollNo;
    }

    public void setUserRollNo(String userRollNo) {
        this.userRollNo = userRollNo;
    }

    public String getUserHostel() {
        return userHostel;
    }

    public void setUserHostel(String userHostel) {
        this.userHostel = userHostel;
    }
}
